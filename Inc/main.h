/**
  ******************************************************************************
  * File Name          : main.h
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2020 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
  /* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define moteur_2_Pin GPIO_PIN_2
#define moteur_2_GPIO_Port GPIOA
#define moteur_1_Pin GPIO_PIN_3
#define moteur_1_GPIO_Port GPIOA
#define arrive_train_Pin GPIO_PIN_4
#define arrive_train_GPIO_Port GPIOC
#define depart_train_Pin GPIO_PIN_5
#define depart_train_GPIO_Port GPIOC
#define DPV1_Pin GPIO_PIN_0
#define DPV1_GPIO_Port GPIOB
#define Voit1_Pin GPIO_PIN_1
#define Voit1_GPIO_Port GPIOB
#define Voit1_EXTI_IRQn EXTI1_IRQn
#define R2_Pin GPIO_PIN_10
#define R2_GPIO_Port GPIOB
#define O2_Pin GPIO_PIN_11
#define O2_GPIO_Port GPIOB
#define V1_Pin GPIO_PIN_12
#define V1_GPIO_Port GPIOB
#define S1_Pin GPIO_PIN_13
#define S1_GPIO_Port GPIOB
#define R1_Pin GPIO_PIN_14
#define R1_GPIO_Port GPIOB
#define O1_Pin GPIO_PIN_15
#define O1_GPIO_Port GPIOB
#define Top_codeurs_Pin GPIO_PIN_5
#define Top_codeurs_GPIO_Port GPIOB
#define DPV2_Pin GPIO_PIN_6
#define DPV2_GPIO_Port GPIOB
#define Voit2_Pin GPIO_PIN_7
#define Voit2_GPIO_Port GPIOB
#define Voit2_EXTI_IRQn EXTI9_5_IRQn
#define V2_Pin GPIO_PIN_8
#define V2_GPIO_Port GPIOB
#define S2_Pin GPIO_PIN_9
#define S2_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

#ifdef ISTP
#define V1_Pin GPIO_PIN_13
#define O1_Pin GPIO_PIN_14
#define R1_Pin GPIO_PIN_15
#define S1_Pin GPIO_PIN_12
#define R2_Pin GPIO_PIN_11
#define O2_Pin GPIO_PIN_10
#define V2_Pin GPIO_PIN_9
#define S2_Pin GPIO_PIN_8
#define DPV1_Pin GPIO_PIN_5
#define DPV2_Pin GPIO_PIN_6
#else
#define V1_Pin GPIO_PIN_12
#define O1_Pin GPIO_PIN_15
#define R1_Pin GPIO_PIN_14
#define S1_Pin GPIO_PIN_13
#define R2_Pin GPIO_PIN_10
#define O2_Pin GPIO_PIN_11
#define V2_Pin GPIO_PIN_8
#define S2_Pin GPIO_PIN_9
#define DPV1_Pin GPIO_PIN_0
#define DPV2_Pin GPIO_PIN_6
#endif

/* USER CODE END Private defines */

void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)

/**
  * @}
  */ 

/**
  * @}
*/ 

#endif /* __MAIN_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
